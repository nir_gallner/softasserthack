//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


import org.apache.commons.lang.exception.ExceptionUtils;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.internal.ArrayComparisonFailure;
import org.junit.internal.ExactComparisonCriteria;
import org.junit.internal.InexactComparisonCriteria;

public class MyAsserts {
    protected MyAsserts() {
    }


    public static void assertEquals(String message, Object expected, Object actual) {
        try {
            Assert.assertEquals(message, expected, actual);
        } catch (Throwable t) {

            // TODO: Change this to writing to log
            System.out.println("expected: " + expected + " actual: " + actual + " message: " + message);
            throw t;
        }
    }


    public static void assertEquals(String message, double expected, double actual, double delta) {
        try {
            Assert.assertEquals(message, expected, actual, delta);
        } catch (Throwable t) {

            // TODO: Change this to writing to log
            System.out.println("expected: " + expected + " actual: " + actual + " message: " + message);
            throw t;
        }
    }

    public static void fail(String message) {
        Assert.fail(message);
    }
}
