import org.apache.commons.lang.exception.ExceptionUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MySoftAssert {

    private List<Throwable> assertions;

    public MySoftAssert() {
        assertions = new ArrayList<>();
    }

    public void assertEquals(String message, Object expected, Object actual) {
        try {
            MyAsserts.assertEquals(message, expected, actual);
        } catch (Throwable t) {
            assertions.add(t);
        }
    }

    public void assertEquals(String message, double expected, double actual, double delta) {
        try {
            MyAsserts.assertEquals(message, expected, actual, delta);
        } catch (Throwable t) {
            assertions.add(t);
        }
    }

    public void assertAll() {
        if (this.assertions.isEmpty())
            return;
        else {
            final Iterator it = assertions.iterator();
            while (it.hasNext()){
                Throwable t = (Throwable)it.next();
                System.out.println(ExceptionUtils.getStackTrace(t));
            }
            MyAsserts.fail(assertions.toString());
        }
    }
}
