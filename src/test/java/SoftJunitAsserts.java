import org.junit.Test;

public class SoftJunitAsserts {

    @Test
    public void softAssertExampleAllPass(){
        MySoftAssert softAssert = new MySoftAssert();
        softAssert.assertEquals("This should be ok", "4", "4");
        softAssert.assertEquals("This should be ok", "6", "6");
        softAssert.assertEquals("This should be ok", "5", "5");
        softAssert.assertAll();
    }

    @Test
    public void softAssertExampleWillFail(){
        MySoftAssert softAssert = new MySoftAssert();
        softAssert.assertEquals("This should be ok", "4", "4");
        softAssert.assertEquals("This should not be ok", "4", "5");
        softAssert.assertEquals("This should be ok", "5", "5");
        softAssert.assertEquals("This should be ok", "6", "6");
        softAssert.assertEquals("This should not be ok", "6", "7");


        softAssert.assertAll();
    }

    @Test
    public void willPassBecauseNoAssertAllInvoked(){
        MySoftAssert softAssert = new MySoftAssert();
        softAssert.assertEquals("This should be ok", "4", "4");
        softAssert.assertEquals("This should not be ok", "4", "5");
        softAssert.assertEquals("This should be ok", "5", "5");
        // softAssert.assertAll();
    }
}
